using System;

using Avi.OpenMod.Database;
using Avi.OpenMod.Unturned.XpEconomy.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Avi.OpenMod.Unturned.XpEconomy.Database {

    internal class XpEconomyDbContextSqlite : AviDbContext {

        public DbSet<XpBalance> XpBalances { get; set; } = null!;

        public XpEconomyDbContextSqlite(IServiceProvider serviceProvider) : base(serviceProvider) { }

        public XpEconomyDbContextSqlite(DbContextOptions options, IServiceProvider serviceProvider) : base(options, serviceProvider) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            const int idMaxLength = 36;
            const int typeMaxLength = 50;

            EntityTypeBuilder<XpBalance> entity = modelBuilder.Entity<XpBalance>();

            entity.HasKey(x => new { x.OwnerId, x.OwnerType });
            entity.Property(x => x.OwnerId).HasMaxLength(idMaxLength).IsRequired();
            entity.Property(x => x.OwnerType).HasMaxLength(typeMaxLength).IsRequired();

            entity.ToTable("XpBalances");

        }

    }

}