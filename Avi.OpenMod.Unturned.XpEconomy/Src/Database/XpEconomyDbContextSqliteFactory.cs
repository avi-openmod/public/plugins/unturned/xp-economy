using Avi.OpenMod.Database;

namespace Avi.OpenMod.Unturned.XpEconomy.Database {

    // ReSharper disable once UnusedType.Global
    internal class XpEconomyDbContextSqliteFactory : AviDbContextFactory<XpEconomyDbContextSqlite> { }

}