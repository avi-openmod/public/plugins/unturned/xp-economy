namespace Avi.OpenMod.Unturned.XpEconomy.Models {

    internal class XpBalance {

        public string OwnerId { get; set; }

        public string OwnerType { get; set; }

        public uint Amount { get; set; }

    }

}