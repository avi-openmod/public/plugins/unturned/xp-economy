using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Autofac;

using Avi.OpenMod.Unturned.XpEconomy.Database;
using Avi.OpenMod.Unturned.XpEconomy.Models;

using Cysharp.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

using OpenMod.API;
using OpenMod.API.Eventing;
using OpenMod.API.Ioc;
using OpenMod.API.Plugins;
using OpenMod.API.Users;
using OpenMod.Core.Plugins.Events;
using OpenMod.Core.Users;
using OpenMod.Extensions.Economy.Abstractions;
using OpenMod.Unturned.Users;
using OpenMod.Unturned.Users.Events;

namespace Avi.OpenMod.Unturned.XpEconomy {

    [ServiceImplementation(Lifetime = ServiceLifetime.Transient)]
    // ReSharper disable once UnusedType.Global
    internal class Service : IEconomyProvider, IDisposable {

        private readonly ILogger<Service> _logger;

        #region Services and ctor

        private readonly IUnturnedUserDirectory _userDirectory;
        private readonly IUserManager _userManager;
        private readonly IRuntime _runtime;
        private readonly IEventBus _eventBus;
        private readonly IPluginAccessor<Plugin> _pluginAccessor;

        private readonly List<IDisposable> _events = new();

        public Service(ILogger<Service> logger, IServiceProvider serviceProvider) {
            _logger = logger;
            _pluginAccessor = serviceProvider.GetRequiredService<IPluginAccessor<Plugin>>();
            _userDirectory = serviceProvider.GetRequiredService<IUnturnedUserDirectory>();
            _userManager = serviceProvider.GetRequiredService<IUserManager>();
            _runtime = serviceProvider.GetRequiredService<IRuntime>();
            _eventBus = serviceProvider.GetRequiredService<IEventBus>();
            _events.Add(_eventBus.Subscribe<PluginConfigurationChangedEvent>(_runtime, (_, _, ev) => {

                    if (ev.Plugin.OpenModComponentId == Plugin.PLUGIN_ID) {
                        OnConfigChanged(ev.Configuration);
                    }

                    return Task.CompletedTask;
                })
            );
            _events.Add(_eventBus.Subscribe<UnturnedUserConnectedEvent>(_runtime,
                (_, _, ev) => SetPersistedXpOnConnect(ev.User))
            );
            _events.Add(_eventBus.Subscribe<UnturnedUserDisconnectedEvent>(_runtime,
                (_, _, ev) => PersistXpOnDisconnect(ev.User))
            );
        }

        public void Dispose()
            => _events.ForEach(x => x.Dispose());

        #endregion

        #region Services resolved from plugin

        private IStringLocalizer? _localizer;

        private IStringLocalizer Localizer => _localizer ??= _pluginAccessor.Instance!.LifetimeScope.Resolve<IStringLocalizer>();

        private XpEconomyDbContextSqlite CreateDbContext() => _pluginAccessor.Instance!.LifetimeScope.Resolve<XpEconomyDbContextSqlite>();

        #endregion

        #region Config

        private bool _allowNonPlayerUsers;
        private bool _allowNonExistingUsers;

        private void OnConfigChanged(IConfiguration cfg) {
            _allowNonPlayerUsers = cfg.GetValue<bool>("allow_non_player_users");
            _allowNonExistingUsers = cfg.GetValue<bool>("allow_non_existing_users");
        }

        #endregion

        #region Economy provider balance overrides

        public async Task<decimal> GetBalanceAsync(string ownerId, string ownerType) {

            // check if we can process this user type
            CheckIfUserIsPlayer(ownerType);

            // try to get balance of online player
            if (ownerType == KnownActorTypes.Player && await GetOnlinePlayerAsync(ownerId) is { } uUser) {
                return await GetPlayerXpAsync(uUser);
            }

            // get offline player
            await CheckIfOfflineUserExists(ownerId, ownerType);

            // get offline player persisted balance
            XpBalance? xpBalance = await GetXpBalanceAsync(ownerId, ownerType);
#if DEBUG
            _logger.LogInformation("Xp balance for {Id}/{Type} null: {Null}", ownerId, ownerType, xpBalance == null);
#endif
            uint savedXp = xpBalance?.Amount ?? 0u;

            return savedXp;
        }

        public async Task<decimal> UpdateBalanceAsync(string ownerId, string ownerType, decimal changeAmount, string? reason) {

            // check if we can process this user type
            CheckIfUserIsPlayer(ownerType);

            // convert change amount to int
            int amount = (int) changeAmount;

            // try to update online player XP
            if (ownerType == KnownActorTypes.Player) {
                UnturnedUser? unturnedUser = await GetOnlinePlayerAsync(ownerId);
                if (unturnedUser != null) {
                    // change online player XP and return new balance
                    return await UpdateBalanceInstanceAsync(ownerId, ownerType,
                        async () => await GetPlayerXpAsync(unturnedUser), amount,
                        async newBalance => await SetPlayerXpAsync(unturnedUser, newBalance),
                        reason
                    );
                }
            }

            // check offline user existence
            await CheckIfOfflineUserExists(ownerId, ownerType);

            // get data fetcher and setter
            (Func<Task<uint>> getOldBalance, Func<uint, Task> setNewBalance) = await GetXpBalanceTasksAsync(ownerId, ownerType);
            // persist data and return new balance
            return await UpdateBalanceInstanceAsync(ownerId, ownerType, getOldBalance, amount, setNewBalance, reason);
        }

        public async Task SetBalanceAsync(string ownerId, string ownerType, decimal balance) {

            // check if we can process this user type
            CheckIfUserIsPlayer(ownerType);

            // convert balance to uint
            uint newBalance = (uint) balance;

            // try to set online player XP
            if (ownerType == KnownActorTypes.Player) {
                UnturnedUser? unturnedUser = await GetOnlinePlayerAsync(ownerId);
                if (unturnedUser != null) {
                    // change online player XP
                    await SetBalanceInstanceAsync(ownerId, ownerType,
                        async () => await GetPlayerXpAsync(unturnedUser),
                        newBalance,
                        async bal => await SetPlayerXpAsync(unturnedUser, bal)
                    );
                    return;
                }
            }

            // check offline user existence
            await CheckIfOfflineUserExists(ownerId, ownerType);

            // get data fetcher and setter
            (Func<Task<uint>> getOldBalance, Func<uint, Task> setNewBalance) = await GetXpBalanceTasksAsync(ownerId, ownerType);
            // persist data
            await SetBalanceInstanceAsync(ownerId, ownerType, getOldBalance, newBalance, setNewBalance);
        }

        private async Task<(Func<Task<uint>>, Func<uint, Task>)> GetXpBalanceTasksAsync(string ownerId, string ownerType) {
            XpBalance xpBalance = await GetXpBalanceAsync(ownerId, ownerType) ?? CreateNewXpBalance(ownerId, ownerType);
            Task<uint> GetOldBalance()
                => Task.FromResult(xpBalance.Amount);
            Task SetNewBalance(uint bal)
                => SaveXpBalanceAsync(xpBalance, bal);
            return (GetOldBalance, SetNewBalance);
        }

        private static XpBalance CreateNewXpBalance(string ownerId, string ownerType)
            => new() { OwnerId = ownerId, OwnerType = ownerType, Amount = 0u };

        #endregion

        #region Economy provider currency overrides

        public string CurrencyName => Localizer["currency_name"];

        public string CurrencySymbol => Localizer["currency_symbol"];

        #endregion

        #region Helpers

        private void CheckIfUserIsPlayer(string ownerType) {

            if (ownerType == KnownActorTypes.Player) {
                return;
            }

            if (!_allowNonPlayerUsers) {
                throw new ArgumentException("Only player users are supported", nameof(ownerType));
            }

        }

        private async Task CheckIfOfflineUserExists(string ownerId, string ownerType) {
            IUser? user = await _userManager.FindUserAsync(ownerType, ownerId, UserSearchMode.FindById);
            if (user == null) {
                if (!_allowNonExistingUsers) {
                    throw new InvalidOperationException($"Player with ID {ownerId} of type {ownerType} doesn't exist, can't get balance");
                }
            }
        }

        private async Task<uint> UpdateBalanceInstanceAsync(
            string ownerId, string ownerType,
            Func<Task<uint>> getBalance,
            int changeAmount,
            Func<uint, Task> setBalance,
            string? reason
        ) {

            uint oldBalance = await getBalance();

            long newExpLong = oldBalance + changeAmount;
            if (newExpLong < 0) {
                throw new NotEnoughBalanceException("Trying to take more XP than player has", oldBalance);
            }

            uint newBalance = (uint) newExpLong;
            await setBalance(newBalance);

            await FireBalanceUpdatedEvent(ownerId, ownerType, oldBalance, newBalance, reason);

            return newBalance;
        }

        private async Task SetBalanceInstanceAsync(
            string ownerId, string ownerType,
            Func<Task<uint>> getBalance,
            uint newBalance,
            Func<uint, Task> setBalance
        ) {
            uint oldBalance = await getBalance();
            await setBalance(newBalance);
            await FireBalanceUpdatedEvent(ownerId, ownerType, oldBalance, newBalance, "Set balance");
        }

        private async Task<XpBalance?> GetXpBalanceAsync(string ownerId, string ownerType)
            => await CreateDbContext().XpBalances.FirstOrDefaultAsync(x => x.OwnerId == ownerId && x.OwnerType == ownerType);

        private async UniTask<UnturnedUser?> GetOnlinePlayerAsync(string ownerId) {
            await UniTask.SwitchToMainThread();
            return _userDirectory.GetOnlineUsers().FirstOrDefault(d => d.Player.SteamId.ToString() == ownerId);
        }

        private static async UniTask<uint> GetPlayerXpAsync(UnturnedUser user) {
            await UniTask.SwitchToMainThread();
            return user.Player.Player.skills.experience;
        }

        private static async UniTask SetPlayerXpAsync(UnturnedUser user, uint newXp) {
            await UniTask.SwitchToMainThread();
            user.Player.Player.skills.ServerSetExperience(newXp);
        }

        private async Task SaveXpBalanceAsync(XpBalance xpBalance, uint newBalance) {

            xpBalance.Amount = newBalance;

            XpEconomyDbContextSqlite dbContext = CreateDbContext();
            dbContext.XpBalances.Update(xpBalance);
            await dbContext.SaveChangesAsync();

        }

        private async Task FireBalanceUpdatedEvent(
            string ownerId, string ownerType,
            uint oldBalance, uint newBalance,
            string? reason
        ) {
            var ev = new BalanceUpdatedEvent(ownerId, ownerType, oldBalance, newBalance, reason);
            await _eventBus.EmitAsync(_runtime, this, ev);
        }

        #endregion

        #region UnturnedUser Events

        private async Task SetPersistedXpOnConnect(UnturnedUser user) {

            XpBalance? xpBalance = await GetXpBalanceAsync(user.Id, user.Type);
            if (xpBalance != null) {
#if DEBUG
                _logger.LogInformation("SetPersistedXpOnConnect: {Id}/{Type}: {Amount}", user.Id, user.Type, xpBalance.Amount);
#endif
                await SetPlayerXpAsync(user, xpBalance.Amount);
            } else {
#if DEBUG
                _logger.LogInformation("SetPersistedXpOnConnect: {Id}/{Type}: no balance", user.Id, user.Type);
#endif
            }

        }

        private async Task PersistXpOnDisconnect(UnturnedUser user) {

            uint newBalance = await GetPlayerXpAsync(user);

            XpBalance xpBalance = CreateNewXpBalance(user.Id, user.Type);
#if DEBUG
            _logger.LogInformation("PersistXpOnDisconnect: {Id}/{Type}: {Amount}", user.Id, user.Type, newBalance);
#endif
            await SaveXpBalanceAsync(xpBalance, newBalance);

        }

        #endregion

    }

}