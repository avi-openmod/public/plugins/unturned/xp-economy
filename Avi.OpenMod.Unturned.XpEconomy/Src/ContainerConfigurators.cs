using Autofac;

using Avi.OpenMod.Database;
using Avi.OpenMod.Unturned.XpEconomy.Database;

using OpenMod.API.Ioc;
using OpenMod.API.Plugins;

// ReSharper disable UnusedType.Global

namespace Avi.OpenMod.Unturned.XpEconomy {

    internal class ContainerConfigurator : IContainerConfigurator {

        public void ConfigureContainer(
            IOpenModServiceConfigurationContext openModStartupContext,
            ContainerBuilder containerBuilder
        ) {
            containerBuilder.AddAviDatabaseSupport();
        }

    }

    internal class PluginContainerConfigurator : IPluginContainerConfigurator {

        public void ConfigureContainer(IPluginServiceConfigurationContext context)
            => context.ContainerBuilder.AddAviDbContext<XpEconomyDbContextSqlite>();

    }

}