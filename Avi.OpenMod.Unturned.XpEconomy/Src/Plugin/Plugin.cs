using System;

using Avi.OpenMod.Unturned.XpEconomy.Database;

using Cysharp.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using OpenMod.API.Plugins;
using OpenMod.Extensions.Economy.Abstractions;
using OpenMod.Unturned.Plugins;

[assembly: PluginMetadata(Avi.OpenMod.Unturned.XpEconomy.Plugin.PLUGIN_ID,
    DisplayName = "Avi XP Economy",
    Author = "aviadmini",
    Website = "https://avi.pw/discord"
)]

namespace Avi.OpenMod.Unturned.XpEconomy {

    // ReSharper disable once ClassNeverInstantiated.Global
    internal class Plugin : OpenModUnturnedPlugin {

        private readonly IServiceProvider _serviceProvider;

        internal const string PLUGIN_ID = "Avi.OpenMod.Unturned.XpEconomy";

        public Plugin(IServiceProvider serviceProvider) : base(serviceProvider)
            => _serviceProvider = serviceProvider;

        protected override async UniTask OnLoadAsync() {

            var economyProvider = _serviceProvider.GetRequiredService<IEconomyProvider>();
            Logger.LogInformation("Economy provider: {Type}", economyProvider.GetType());

            await _serviceProvider.GetRequiredService<XpEconomyDbContextSqlite>().Database.MigrateAsync();

        }

    }

}