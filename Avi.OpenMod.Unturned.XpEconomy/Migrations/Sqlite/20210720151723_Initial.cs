﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Avi.OpenMod.Unturned.XpEconomy.Migrations.Sqlite
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "XpBalances",
                columns: table => new
                {
                    OwnerId = table.Column<string>(maxLength: 36, nullable: false),
                    OwnerType = table.Column<string>(maxLength: 50, nullable: false),
                    Amount = table.Column<uint>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_XpBalances", x => new { x.OwnerId, x.OwnerType });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "XpBalances");
        }
    }
}
