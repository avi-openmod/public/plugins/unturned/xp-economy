Avi.OpenMod.Unturned.XpEconomy plugin
-------------------------------------

Economy provider for OpenMod.Unturned that uses player experience (XP) as currency.

XpEconomy is designed to work with any other plugin, including working with offline/non-existent players and non-player user types. Those options can be turned on in `config.yaml` but it's recommended to turn them off when they're unused.

This package **does not include commands**. Please install economy commands separately, like [Avi.OpenMod.Economy.Commands package](https://www.nuget.org/packages/Avi.OpenMod.Economy.Commands)

#### Config
```yaml
# Allows working with non-existing users
# Generally this should be off, but some plugins might need it on
allow_non_existing_users: false
# Allows saving XP for users with non-player type
# Generally this should be off, but some plugins might need it on
allow_non_player_users: false
```

#### Installation:

Run `openmod install Avi.OpenMod.Unturned.XpEconomy` to install/update the plugin.
To install beta versions use `om install Avi.OpenMod.Unturned.XpEconomy -Pre`.

License
=======

    Copyright 2021 aviadmini

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.